(function($){

	$(document).on('click','[data-postCargar]',function(e){
	 	e.preventDefault();
	 	id = $(this).attr('data-postCargar');

		injectPostPanelsCSS(id);


		$.ajax({
			url : fkm_vars.ajaxurl,
			type: 'post',
			data: {
				action : 'fkm_ajax',
				id_post: id
			},
			beforeSend: function(){
				//link.html('Cargando ...');
				$('.postPopup').fadeIn().html("Cargando...");
			},
			success: function(resultado){

				$('.postPopup').fadeIn().html(resultado);
			}

		});

	});

	$(document).on('click','.cerrarPost, .postPopup',function(e){
		if(e.target != this) return; // Solo continuar si se dio click exactamente en el selector
		$('.postPopup').fadeOut();
	});

})(jQuery);
