<?php
/**
 * @package Componente
*/
/*
Plugin Name: Componente
Plugin URI: http://www.colombiavip.com/
Description: Componente para essential grid
Version: 1.0.0
Author: Jonathan Giraldo
*/
defined ('ABSPATH') or die ("Ey!, que estas haciendo aqui? humano tonto >:(");

if (!class_exists('Componente')){
  class Componente {
    function __construct(){
      add_action('wp_footer', 'postPopup');
      function postPopup() {
          echo "<div class='postPopup'></div>";
      }

      //Crear AJAX
      add_action('wp_ajax_nopriv_fkm_ajax', array($this, 'mostrarPost'));
      add_action('wp_ajax_fkm_ajax', array($this, 'mostrarPost'));
      add_action('wp_enqueue_scripts', array($this, 'enqueue'));

      //activacion
      register_activation_hook(__FILE__, array($this, 'activate'));

      //desactivacion

      register_deactivation_hook(__FILE__, array($this, 'deactivate'));
      //requerir archivo
       //require_once plugin_dir_path(__FILE__) . 'assets/confirm-deleted.php';

    }

    function register(){

    }

    function enqueue(){
      wp_enqueue_style('fkm_style', plugins_url('/inc/pluginStyle.css?4', __FILE__));
      wp_register_script('fkm_miscript', plugins_url('/inc/myscriptplugin.js?2', __FILE__), array('jquery'), '1', true);
      wp_enqueue_script('fkm_miscript');
      wp_localize_script('fkm_miscript', 'fkm_vars', ['ajaxurl'=>admin_url('admin-ajax.php')]);
    }

    function mostrarPost(){
        if(empty($_POST['id_post']) OR !is_numeric($_POST['id_post']))
          die("No sea rata mano");

        $id_post = $_POST['id_post'];

        $titulo = get_the_title($id_post);

        $content_post = get_post($id_post);
        // $content = strip_shortcodes($content_post->post_content);
        // $gallery = get_post_gallery($id_post, FALSE);

        $content = apply_filters('the_content',$content_post->post_content);



        ob_start();
				require 'inc/vista.php';
        echo ob_get_clean();

        die();
    }

    function activate(){

    }



    function deactivate(){
      flush_rewrite_rules();
    }

  }

  $componente = new Componente();



}
