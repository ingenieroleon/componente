#COMPONENTE ajax

Este plugin de WordPress permite que cualquier vinculo pueda cargar un post en un lightbox.

## Como activarlo
- Subir el folder a la carpeta plugins de WordPress.
- Activar el plugin en el admin de WordPress.
## Como usarlo
- Puedes agregar a cualquier objeto del HTML el atributo data-postCargar con el ID del POST como valor ejemplo:
  <div data-postCargar="123"></div>
- Tus usuarios al clickear el link veran el post en cuestion en un lightbox.
